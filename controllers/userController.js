const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt =require("bcrypt");
const auth = require("../auth");





//checking if the email exist in the database
module.exports.checkEmailExist =(reqBody)=>{

	return User.find({email:reqBody.email}).then(result =>{

		if (result.length >0){
			return true
		}
		//no duplicate email found //user is not yety registerd in our database
		else{
			return false
		}

	})
}


module.exports.registerUser =(reqBody)=>{

	let newUser = new User ({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo:reqBody.mobileNo,
		//bycrypt.hashSync(<dataToBeHash>, <saltRound>)
		password:bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error)=>{

		if (error){
			return false
		}else{
			return user
		}


	})

}


module.exports.loginUser =(reqBody)=>{


	return User.findOne({email: reqBody.email}).then(result=>{

		console.log(result)

		if(result==null){
			return false
		}else{
			//compareSync(dataFromRequestBody, encryptedDataFromDatabase)
			const isPasswordCorrect =bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}
			else{
				return false
			}
		}

	})

}



//fix this later 
module.exports.getProfile =(data)=>{

	return User.findById(data.id).then(result=>{
		result.password="";
		return result;
	})
/*	return User.findOne({_id: reqBody._id}).then(result=>{
			if (result==null){
				return "user id not exist"
			}
			else{
				return result.password="", result
			}

	})*/

}



//enroll user to a course

module.exports.enroll = async(data)=>{
	//adds the courseId ion the user's enrollement array
	let isUserUpdated = await User.findById(data.userId).then(user=>{
		user.enrollments.push({courseId: data.courseId});
		return user.save().then ((user, error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});

	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course=>{
		//add the userId in the course's enrollees array
		course.enrollees.push({userId: data.userId});
		return course.save().then ((course, error)=>{
			if (error){
				return false;
			}else{
				return true;
			};
		});
	});
	//condition that will check if the user and couse documents have been updated
	if (isUserUpdated && isCourseUpdated){
		//success enrollment
		return true
	}else{
		//failed enrollment
		return false
	};
};
















