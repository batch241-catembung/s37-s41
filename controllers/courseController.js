const Course = require("../models/Course");

module.exports.addCourse = (reqBody) => {


	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})
}


// retrieve all courses
module.exports.getAllCourses = () =>{

	return Course.find({}).then(result=>{
		return result;
	});

};


//retrieve all courses active only

module.exports.getAllActiveCourses = () =>{

	return Course.find({isActive: true}).then(result=>{
		return result;

	});

};


//get specific course 

module.exports.getCourse = (reqParams) =>{

	return Course.findById(reqParams.courseId).then(result=>{
		return result;
	})
	

};

//update course
module.exports.updateCourse=(reqParams, reqBody)=>{

	let updateCourse={
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	/*
		syntax:
		findByIdAndUpdate(documentID, updatesToBeApplied)
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse)
	.then((course, error)=>{
		if (error){
			return false;
		}else{
			return true;
		}
	});
};

//archive Course
module.exports.archiveCourse=(reqParams, reqBody)=>{

	let updateCourse={
		isActive: reqBody.isActive,
	};
	
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse)
	.then((course, error)=>{
		if (error){
			return false;
		}else{
			return true;
		}
	});
};



