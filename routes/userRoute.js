const express =require("express");

const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

//Route for checking if the user's email already exist in our database
router.post("/checkEmail", (req, res)=>{

	userController.checkEmailExist(req.body).then(resultFromController=>res.send(resultFromController));

});

//route for user registration
router.post("/register", (req, res)=>{

	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));

});

//route for user authentication
router.post("/login", (req, res)=>{

	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));

})

//activty s38
router.post("/details", auth.verify, (req, res)=>{

	const userData = auth.decode(req.headers.authorization);

userController.getProfile({id: userData.id}).then(resultFromController=>res.send(resultFromController));


/*	userController.getProfile(req.body).then(resultFromController=>res.send(resultFromController));
*/
})

//route to enroll user
// router.post("/enroll", (req, res)=>{
// 	 let data ={
// 	 	userId: reqBody.userId,
// 	 	courseId: reqBody.courseId
// 	 };
// 	userController.enroll(data).then(resultFromController=>res.send(resultFromController));

// })

//route to enroll user
router.post("/enroll", auth.verify, (req, res)=>{
	 let data ={
	 	userId: auth.decode(req.headers.authorization).id,
	 	courseId: req.body.courseId
	 };
	userController.enroll(data).then(resultFromController=>res.send(resultFromController));

})


module.exports = router;













