const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");



//Route for creating a course
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin == true){
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
	}
	
});


//course retrieve all 
router.get("/all", (req, res)=>{
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
});
//get active course only
router.get("/active", (req, res)=>{
	courseController.getAllActiveCourses().then(resultFromController => res.send(resultFromController))
});


//get specific course 
router.get("/:courseId", (req, res)=>{
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
});

//update a course
router.put("/:courseId", auth.verify, (req, res)=>{
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
});

//archive course
router.patch("/:courseId/archive", auth.verify, (req, res)=>{
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
});



module.exports = router;
